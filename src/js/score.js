export function win() {
let width = document.querySelector('body').offsetWidth;
let height = document.querySelector('body').offsetHeight;
let div = document.createElement('div');
let title = document.createElement('h1');
let p = document.createElement('p');
let retry = document.createElement('p');
let img = document.createElement('img')

width = width - 200;

height = height - 120;


div.style.width = width + "px";
div.style.height = height + "px";
div.classList.add('messageEnd');

title.textContent= "BRAVO ";
p.textContent = "Vous avez gagné la partie!"
retry.textContent = "Cliques-ici pour rejouer ! "
retry.setAttribute("id", "remove");
retry.style.cursor = "pointer";

img.src = './smile.png'
img.setAttribute("id", "remove");
img.style.width = '750px';
img.style.height = '400px';

retry.addEventListener('click', function () {
 location.reload();
})


document.body.appendChild(div);
div.appendChild(title);
div.appendChild(p);
div.appendChild(retry);
div.appendChild(img);
}



export function lose() {
let width = document.querySelector('body').offsetWidth;
let height = document.querySelector('body').offsetHeight;
let div = document.createElement('div');
let title = document.createElement('h1');
let p = document.createElement('p');
let retry = document.createElement('p');
let img = document.createElement('img')

width = width - 200;

height = height - 120;


div.style.width = width + "px";
div.style.height = height + "px";
div.classList.add('messageEnd');

title.textContent= "PERDU ";
p.textContent = "Retente ta chance !"
retry.textContent = "Cliques-ici pour rejouer ! "
retry.setAttribute("id", "remove");
retry.style.cursor = "pointer";

img.src = './sad.png'
img.setAttribute("id", "remove");
img.style.width = '500px';
img.style.height = '450px';

retry.addEventListener('click', function () {
 location.reload();
})


document.body.appendChild(div);
div.appendChild(title);
div.appendChild(p);
div.appendChild(retry);
div.appendChild(img);
}